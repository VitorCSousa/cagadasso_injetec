@component('mail::message')
    # Contato via Site

    Recebemos um contato via formulário, segue os detalhes.

    Nome: {{ $request['nome'] }}<br>
    E-mail: {{ $request['email'] }}<br>
    Telefone: {{ $request['telefone'] }}<br>

    @if(isset($request['mensagem']) && $request['mensagem'])
        Mensagem:<br>
        {{ $request['mensagem'] }}
    @endif
    <br>
    {{ config('app.name') }}
@endcomponent

