@component('mail::message')
    # Currículo recebido via Site!

    Recebemos um curriculo via formulário, abaixo segue um resumo das informações.

    Nome: {{ $request['nome'] }}<br>
    E-mail: {{ $request['email'] }}<br>


    @component('mail::button', ['url' => $url])
        View Order
    @endcomponent

    <br>
    {{ config('app.name') }}
@endcomponent
