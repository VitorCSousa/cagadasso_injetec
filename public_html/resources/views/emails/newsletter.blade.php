@component('mail::message')
    # Interesse em receber notícias!

    Recebemos um interesse via formulário em receber novidades e notícias da Injetec! Segue abaixo os detalhes.

    Nome: {{ $request['nome'] }}<br>
    E-mail: {{ $request['email'] }}<br>
    <br>
    {{ config('app.name') }}
@endcomponent
