<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('generate', function (){
    \Illuminate\Support\Facades\Artisan::call('storage:link');
    echo 'ok';
});

Route::get('/','App\Http\Controllers\Site\SiteController@recepcao')->name('site.recepcao');
Route::name('site.recepcao.')->prefix('recepcao')->namespace('App\Http\Controllers\Site')->group(function (){
    Route::get('institucional','SiteController@recepcaoInstitucional')->name('institucional');
    Route::get('ecommerce','SiteController@recepcaoEcommerce')->name('ecommerce');
    Route::get('area-cliente','SiteController@recepcaoAreaCliente')->name('areaCliente');
});

Route::namespace('App\Http\Controllers\Site')->prefix('institucional')->name('site.')->group(function (){
    Route::get('/','SiteController@index')->name('index');
    Route::get('quem-somos','SiteController@quemSomos')->name('quemSomos');
    Route::get('blog','SiteController@blog')->name('blog');
    Route::get('noticia/{slug}','SiteController@noticia')->name('noticia');
    Route::get('contato','SiteController@contato')->name('contato');
    Route::get('trabalheConosco','SiteController@trabalheConosco')->name('trabalheConosco');
    Route::post('enviarMensagem','SiteController@enviarMensagem');
    Route::post('cadastrarNewsletter','SiteController@cadastrarNewsletter');
    Route::post('enviarCurriculo','SiteController@enviarCurriculo');
});


Route::middleware(['auth:sanctum', 'verified'])->namespace('App\Http\Controllers\Painel')->prefix('painel')->name('painel.')->group(function () {
    Route::get('/',function () {
        return Inertia\Inertia::render('Dashboard');
    })->name('dashboard');


    Route::resource('noticias','NoticiasController');
    Route::get('listarNoticias','NoticiasController@listar');

    Route::resource('mensagens','MensagensController');
    Route::get('listarMensagens','MensagensController@listar');

    Route::resource('newsletter','NewsletterController');
    Route::get('listarNewsletter','NewsletterController@listar');

    Route::resource('curriculos','TrabalheConoscoController');
    Route::get('listarCurriculos','TrabalheConoscoController@listar');

    Route::resource('configuracoes','ConfiguracoesController');

    Route::resource('slides', 'SlideController');
    Route::get('listarSlides', 'SlideController@listar');
});
