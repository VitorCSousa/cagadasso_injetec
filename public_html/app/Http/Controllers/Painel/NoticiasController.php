<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Noticia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Inertia\Inertia;

class NoticiasController extends Controller
{
    public function index(){
        return Inertia::render('Painel/Noticias');
    }

    public function listar(){
        try {

            $noticias = Noticia::orderBy('id','DESC')->get();

            return response()->json([
                'success' => true,
                'data' => $noticias
            ]);

        } catch (\Exception $e){
            return response()->json([
                'success' => false
            ]);
        }
    }

    public function store(Request $request){
        try {

            $validar = Validator::make($request->all(),[
                'titulo'=> 'required',
                'noticia' => 'required',
            ]);
            if($validar->fails()){
                return response()->json([
                    'success' => false,
                    'data' => $validar->errors()
                ],406);
            }

            $noticia = new Noticia();
            $noticia->user_id = Auth::user()->id;
            $noticia->fill($request->all());
            $noticia->slug = Str::slug($request->titulo);


            if ($request->hasFile('imagem_upload') && $request->file('imagem_upload')->isValid()) {

                $name = uniqid(date('HisYmd'));

                $extension = $request->imagem_upload->extension();

                $nameFile = "{$name}.{$extension}";

                $upload = Storage::putFileAs('imagem',$request->imagem_upload,$nameFile,'public');

                $noticia->imagem = $upload;


                if ( !$upload )
                    return response()->json([
                        'success' => false,
                        'data' => 'Falha ao fazer upload'
                    ],406);

            }

            $noticia->save();

            return response()->json([
                'success' => true,
                'data' => $noticia
            ]);

        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'data' => $e->getMessage()
            ],500);
        }
    }

    public function show($id){
        try {

            $noticia = Noticia::find($id);

            return response()->json([
                'success' => true,
                'data' => $noticia
            ]);

        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'data' => $e->getMessage()
            ],500);
        }
    }

    public function update(Request $request,$id){
        try {

            $validar = Validator::make($request->all(),[
                'titulo'=> 'required',
                'noticia' => 'required',
            ]);
            if($validar->fails()){
                return response()->json([
                    'success' => false,
                    'data' => $validar->errors()
                ],406);
            }

            $noticia = Noticia::find($id);
            $noticia->fill($request->all());
            $noticia->slug = Str::slug($request->titulo);


            if($request->imagem == null || $request->imagem == 'null'){

                if(File::exists($noticia->imagem)) {
                    $noticia->imagem = null;
                    File::delete($noticia->imagem);
                }
            }

            if ($request->hasFile('imagem_upload') && $request->file('imagem_upload')->isValid()) {


                $nome = uniqid(date('HisYmd'));
                $nome = $nome.'.'.$request->file('imagem_upload')->extension();
                $destinationPath = public_path('/uploads/noticias/'.$noticia->id);
                $image = $request->file('imagem_upload');
                $image->move($destinationPath, $nome);
                //$url = Storage::putFileAs('/uploads/slides/'.$slide->id,$request->imagem_upload,);
                $noticia->imagem = '/uploads/noticias/'.$noticia->id.'/'.$nome;
                //$url = Storage::putFileAs('/uploads/slides/'.$slide->id,$request->imagem_upload_mob,$nome.'.'.$request->file('imagem_upload_mob')->extension());
                //$slide->imagem_mob = $url;
                $noticia->saveOrFail();






            }

            $noticia->save();

            return response()->json([
                'success' => true,
                'data' => $noticia
            ]);

        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'data' => $e->getMessage()
            ],500);
        }
    }
    public function destroy(Request $request,$id){
        try {


            $noticia = Noticia::find($id);
            $noticia->delete();

            return response()->json([
                'success' => true,
                'data' => $noticia
            ]);

        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'data' => $e->getMessage()
            ],500);
        }
    }
}
