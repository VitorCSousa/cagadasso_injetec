<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Newsletter;
use Illuminate\Http\Request;
use Inertia\Inertia;

class NewsletterController extends Controller
{
    public function index(){
        return Inertia::render('Painel/Newsletter');
    }
    public function listar(){
        try {

            $newsletters = Newsletter::orderBy('id','DESC')->get();

            return response()->json([
                'success' => true,
                'data' => $newsletters
            ]);

        } catch (Exception $e){
            return response()->json([
                'success' => false
            ]);
        }
    }
    public function destroy($id){
        try {


            $newsletter = Newsletter::find($id);
            $newsletter->delete();

            return response()->json([
                'success' => true,
                'data' => $newsletter
            ]);

        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'data' => $e->getMessage()
            ],500);
        }
    }
}
