<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Configuracoes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class ConfiguracoesController extends Controller
{

    public function index()
    {
        $dados = Configuracoes::find(1);
        return Inertia::render('Painel/Configuracoes',['configs' => $dados]);
    }

    public function update(Request $request, $id)
    {
        try {

            $validar = Validator::make($request->all(),[
                'email'=> 'required',
                'telefone' => 'required',
                'cep' => 'required',
                'endereco' => 'required',
                'numero' => 'required',
                'bairro' => 'required',
                'cidade' => 'required',
                'estado' => 'required',
                'quem_somos' => 'required',

            ]);
            if($validar->fails()){
                return response()->json([
                    'success' => false,
                    'data' => $validar->errors()
                ],406);
            }

            $dados = Configuracoes::find(1);
            $dados->fill($request->all());


            if ($request->hasFile('arquivo_upload') && $request->file('arquivo_upload')->isValid()) {

                $name = uniqid(date('HisYmd'));

                $extension = $request->arquivo_upload->extension();

                $nameFile = "{$name}.{$extension}";

                $upload = Storage::putFileAs('/uploads/catalogo',$request->arquivo_upload,$nameFile,'public');

                $dados->catalogo = $upload;

                if ( !$upload )
                    return response()->json([
                        'success' => false,
                        'data' => 'Falha ao fazer upload'
                    ],406);

            }
            $dados->save();

            return response()->json([
                'success' => true,
                'data' => $dados
            ]);

        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'data' => $e->getMessage()
            ],500);
        }
    }
}
