<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Noticia;
use App\Models\TrabalheConosco;
use Illuminate\Http\Request;
use Inertia\Inertia;

class TrabalheConoscoController extends Controller
{
    public function index(){
        return Inertia::render('Painel/TrabalheConosco');
    }
    public function listar(){
        try {

            $mensagens = TrabalheConosco::orderBy('id','DESC')->get();

            return response()->json([
                'success' => true,
                'data' => $mensagens
            ]);

        } catch (Exception $e){
            return response()->json([
                'success' => false
            ]);
        }
    }
    public function show($id){
        try {

            $curriculo = TrabalheConosco::find($id);

            return response()->json([
                'success' => true,
                'data' => $curriculo
            ]);

        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'data' => $e->getMessage()
            ],500);
        }
    }
    public function destroy($id){
        try {


            $mensagem = TrabalheConosco::find($id);
            $mensagem->delete();

            return response()->json([
                'success' => true,
                'data' => $mensagem
            ]);

        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'data' => $e->getMessage()
            ],500);
        }
    }
}
