<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Mensagem;
use Illuminate\Http\Request;
use Inertia\Inertia;

class MensagensController extends Controller
{
    public function index(){
        return Inertia::render('Painel/Mensagens');
    }
    public function listar(){
        try {

            $mensagens = Mensagem::orderBy('id','DESC')->get();

            return response()->json([
                'success' => true,
                'data' => $mensagens
            ]);

        } catch (Exception $e){
            return response()->json([
                'success' => false
            ]);
        }
    }
    public function destroy($id){
        try {


            $mensagem = Mensagem::find($id);
            $mensagem->delete();

            return response()->json([
                'success' => true,
                'data' => $mensagem
            ]);

        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'data' => $e->getMessage()
            ],500);
        }
    }
}
