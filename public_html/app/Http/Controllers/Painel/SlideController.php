<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Slide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Inertia\Inertia;

class SlideController extends Controller
{
    public function index(){
        return Inertia::render('Painel/Slide');
    }

    public function listar(Request $request){
        try {

            $slides = Slide::orderBy('id','desc');
            if(isset($request->titulo) && $request->titulo != null){
                $slides = $slides->where('titulo', 'LIKE', '%'.$request->titulo.'%');
            }

            $slides = $slides->get();

            return response()->json([
                'success' => true,
                'data' => $slides
            ]);

        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function show($id){
        try {

            $slide = Slide::find($id);

            return response()->json([
                'success' => true,
                'data' => $slide
            ]);

        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function store(Request $request) {
        try {

            $validar = Validator::make($request->all(),[
                'titulo'=>'required',
            ]);



            if($validar->fails()){
                return response()->json([
                    'success' => false,
                    'errors' => $validar->errors()
                ],500);
            }



            $slide = new Slide();
            $slide->fill($request->all());
            $slide->saveOrFail();

            if(isset($request->imagem_upload) && $request->imagem_upload != null && $request->imagem_upload != 'undefined'){
                $nome = md5(date('dmYHis'));
                $nome = $nome.'.'.$request->file('imagem_upload')->extension();
                $destinationPath = public_path('/uploads/slides/'.$slide->id);
                $image = $request->file('imagem_upload');
                $image->move($destinationPath, $nome);
                //$url = Storage::putFileAs('/uploads/slides/'.$slide->id,$request->imagem_upload,);
                $slide->imagem = '/uploads/slides/'.$slide->id.'/'.$nome;
                $slide->saveOrFail();
            }

            if(isset($request->imagem_upload_mob) && $request->imagem_upload_mob != null && $request->imagem_upload_mob != 'undefined'){
                $nome = md5(date('dmYHis'));
                $nome = $nome.'mob.'.$request->file('imagem_upload_mob')->extension();
                $destinationPath = public_path('/uploads/slides/'.$slide->id);
                $image = $request->file('imagem_upload_mob');
                $image->move($destinationPath, $nome);
                //$url = Storage::putFileAs('/uploads/slides/'.$slide->id,$request->imagem_upload,);
                $slide->imagem_mob = '/uploads/slides/'.$slide->id.'/'.$nome;
                //$url = Storage::putFileAs('/uploads/slides/'.$slide->id,$request->imagem_upload_mob,$nome.'.'.$request->file('imagem_upload_mob')->extension());
                //$slide->imagem_mob = $url;
                $slide->saveOrFail();
            }

            return response()->json([
                'success' => true,
                'message' => $slide
            ]);

        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }
    public function update(Request $request,$id) {
        try {
            $validar = Validator::make($request->all(),[
                'titulo'=>'required',
                'descricao'=>'required',
                'url'=>'required'
            ]);



            if($validar->fails()){
                return response()->json([
                    'success' => false,
                    'errors' => $validar->errors()
                ],500);
            }



            $slide = Slide::find($id);
            $slide->fill($request->all());


            if($request->imagem == null){

                if(File::exists($slide->imagem)) {
                    $slide->imagem = null;
                    File::delete($slide->imagem);
                }
            }

            if($request->imagem_mob == null){

                if(File::exists($slide->imagem_mob)) {
                    $slide->imagem_mob = null;
                    File::delete($slide->imagem_mob);
                }
            }

            $slide->saveOrFail();

            if(isset($request->imagem_upload) && $request->imagem_upload != null && $request->imagem_upload != 'undefined'){
                $nome = md5(date('dmYHis'));
                $nome = $nome.'.'.$request->file('imagem_upload')->extension();
                $destinationPath = public_path('/uploads/slides/'.$slide->id);
                $image = $request->file('imagem_upload');
                $image->move($destinationPath, $nome);
                //$url = Storage::putFileAs('/uploads/slides/'.$slide->id,$request->imagem_upload,);
                $slide->imagem = '/uploads/slides/'.$slide->id.'/'.$nome;
                $slide->saveOrFail();
            }

            if(isset($request->imagem_upload_mob) && $request->imagem_upload_mob != null && $request->imagem_upload_mob != 'undefined'){
                $nome = md5(date('dmYHis'));
                $nome = $nome.'mob.'.$request->file('imagem_upload_mob')->extension();
                $destinationPath = public_path('/uploads/slides/'.$slide->id);
                $image = $request->file('imagem_upload_mob');
                $image->move($destinationPath, $nome);
                //$url = Storage::putFileAs('/uploads/slides/'.$slide->id,$request->imagem_upload,);
                $slide->imagem_mob = '/uploads/slides/'.$slide->id.'/'.$nome;
                //$url = Storage::putFileAs('/uploads/slides/'.$slide->id,$request->imagem_upload_mob,$nome.'.'.$request->file('imagem_upload_mob')->extension());
                //$slide->imagem_mob = $url;
                $slide->saveOrFail();
            }

            return response()->json([
                'success' => true,
                'message' => $slide
            ]);

        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }
    public function destroy($id){
        try {

            $slide = Slide::find($id);


            if(File::exists($slide->imagem)) {
                $slide->imagem = null;
                File::delete($slide->imagem);
            }


            if(File::exists($slide->imagem_mob)) {
                $slide->imagem_mob = null;
                File::delete($slide->imagem_mob);
            }

            $slide->delete();

            return response()->json([
                'success' => true,
            ]);

        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }
}
