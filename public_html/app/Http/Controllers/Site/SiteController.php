<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Painel\ConfiguracoesController;
use App\Models\Mensagem;
use App\Models\Newsletter;
use App\Models\Noticia;
use App\Models\Slide;
use App\Models\TrabalheConosco;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class SiteController extends Controller
{

    // RECEPÇÃO
    public function recepcao() {
        return Inertia::render('Site/Recepcao/Index');
    }
    public function recepcaoInstitucional() {
        return Inertia::render('Site/Recepcao/Institucional');
    }
    public function recepcaoEcommerce() {
        return Inertia::render('Site/Recepcao/Ecommerce');
    }
    public function recepcaoAreaCliente() {
        return Inertia::render('Site/Recepcao/AreaCliente');
    }

    // SITE INSTITUCIONAL
    public function index() {
        $slides = Slide::all();
        $noticia = Noticia::latest()->first();
        return Inertia::render('Site/Index', ['slides' => $slides,'noticia' => $noticia]);
    }
    public function quemSomos() {
        return Inertia::render('Site/QuemSomos');
    }
    public function blog() {
        $noticias = Noticia::all();
        return Inertia::render('Site/Blog', ['noticias' => $noticias]);
    }
    public function noticia($slug) {

        return Inertia::render('Site/Noticia');
    }
    public function contato() {
        return Inertia::render('Site/Contato');
    }
    public function trabalheConosco() {
        return Inertia::render('Site/TrabalheConosco');
    }

    public function enviarMensagem(Request $request){

        try {

            $validar = Validator::make($request->all(), [
                'nome' => 'required',
                'email' => 'required|email',
                'telefone' => 'required',
                'mensagem' => 'required'
            ]);

            if($validar->fails()){
                return response()->json([
                    'success' => false,
                    'error' => $validar->errors()
                ], 500);
            }


            $mensagem = new Mensagem();
            $mensagem->fill($request->all());
            $mensagem->saveOrFail();

            return response()->json([
                'success' => true
            ]);

        }catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], 500);
        }

    }

    public function cadastrarNewsletter(Request $request){

        try {

            $validar = Validator::make($request->all(), [
                'nome' => 'required',
                'email' => 'required|email',
            ]);

            if($validar->fails()){
                return response()->json([
                    'success' => false,
                    'error' => $validar->errors()
                ], 500);
            }


            $newsletter = new Newsletter();
            $newsletter->fill($request->all());
            $newsletter->saveOrFail();

            return response()->json([
                'success' => true
            ]);

        }catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], 500);
        }

    }

    public function enviarCurriculo(Request $request){

        try {

            $validar = Validator::make($request->all(), [
                'nome' => 'required',
                'email' => 'required|email',
                'data_nascimento' => 'required',
                'sexo' => 'required',
                'cep' => 'required',
                'endereco' => 'required',
                'telefone' => 'required',
                'escolaridade' => 'required',
                'motivacao' => 'required'
            ]);

            if($validar->fails()){
                return response()->json([
                    'success' => false,
                    'error' => $validar->errors()
                ], 500);
            }

            $curriculo = new TrabalheConosco();
            $curriculo->fill($request->all());

            if ($request->hasFile('arquivo') && $request->file('arquivo')->isValid()) {

                $nome = md5(date('dmYHis'));
                $nome = $nome.'.'.$request->file('arquivo')->extension();
                $destinationPath = public_path('/uploads/curriculos/');
                $image = $request->file('arquivo');
                $image->move($destinationPath, $nome);
                //$url = Storage::putFileAs('/uploads/slides/'.$slide->id,$request->imagem_upload,);
                $curriculo->anexo = '/uploads/curriculos/'.$nome;


            }

            $curriculo->saveOrFail();

            return response()->json([
                'success' => true
            ]);

        }catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], 500);
        }

    }


}
