<?php

namespace App\Http\Middleware;

use App\Models\Configuracoes;
use Inertia\Inertia;

class InertiaShared
{
    public function handle($request, $next)
    {
        $dados = Configuracoes::find(1);
        Inertia::share([
            'dados' => $dados,
        ]);

        return $next($request);
    }
}
