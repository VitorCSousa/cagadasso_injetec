<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrabalheConosco extends Model
{

    protected $fillable = ['nome','email','data_nascimento','sexo','cep','endereco','numero','bairro','telefone','escolaridade','estuda_atualmente','cursos_realizados','area_interesse','horario_interesse','motivacao','nome_empresa','contato','sua_funcao','trabalhou_de','trabalhou_ate','ultimo_salario','anexo'];
    protected $casts = ['trabalhou_de' => 'date'];
}
