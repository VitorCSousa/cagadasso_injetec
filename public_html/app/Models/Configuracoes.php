<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Configuracoes extends Model
{
    use HasFactory;
    protected $fillable = ['email','telefone','cep','endereco','numero','complemento','bairro','cidade','estado','facebook','instagram','quem_somos'];
}
