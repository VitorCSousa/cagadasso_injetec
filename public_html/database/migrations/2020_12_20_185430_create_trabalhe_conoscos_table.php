<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrabalheConoscosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trabalhe_conoscos', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('email');
            $table->date('data_nascimento');
            $table->string('sexo');
            $table->string('cep');
            $table->string('endereco');
            $table->string('numero')->nullable();
            $table->string('bairro');
            $table->string('telefone');
            $table->string('escolaridade');
            $table->string('estuda_atualmente')->nullable();
            $table->string('cursos_realizados')->nullable();
            $table->string('area_interesse')->nullable();
            $table->string('horario_interesse')->nullable();
            $table->string('motivacao');
            $table->string('nome_empresa')->nullable();
            $table->string('contato')->nullable();
            $table->string('sua_funcao')->nullable();
            $table->date('trabalhou_de')->nullable();
            $table->date('trabalhou_ate')->nullable();
            $table->string('ultimo_salario')->nullable();
            $table->string('anexo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trabalhe_conoscos');
    }
}
